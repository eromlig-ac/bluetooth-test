package com.example.capiral.testapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.StringBuilderPrinter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.douglasjunior.bluetoothclassiclibrary.BluetoothService;
import com.github.douglasjunior.bluetoothclassiclibrary.BluetoothStatus;
import com.github.douglasjunior.bluetoothclassiclibrary.BluetoothWriter;

public class MessagingActivity extends AppCompatActivity implements View.OnClickListener, BluetoothService.OnBluetoothEventCallback {
    private static final String TAG = "MessagingActivity";

    private Button sendButton;
    private TextView logTextView;
    private ScrollView logScrollView;
    private EditText messageEditText;

    private BluetoothService service;
    private BluetoothWriter writer;

    private StringBuilder logBuilder;
    private StringBuilderPrinter log;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messaging);
        initialize();
    }

    private void initialize() {
        sendButton = (Button) findViewById(R.id.sendButton);
        logTextView = (TextView) findViewById(R.id.logTextView);
        logScrollView = (ScrollView) findViewById(R.id.logScrollView);
        messageEditText = (EditText) findViewById(R.id.messageEditText);

        // OnClickListeners
        sendButton.setOnClickListener(this);

        service = BluetoothService.getDefaultInstance();
        writer = new BluetoothWriter(service);

        logBuilder = new StringBuilder();
        log = new StringBuilderPrinter(logBuilder);
    }

    @Override
    protected void onResume() {
        super.onResume();
        service.setOnEventCallback(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        service.disconnect();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.sendButton:
                String text = messageEditText.getText().toString();
                messageEditText.setText("");
                writer.writeln(text);
                break;
            default:
                break;
        }
    }

    @Override
    public void onDataRead(byte[] buffer, int length) {
        Log.d(TAG, "onDataRead " + buffer);
        log.println("< " + new String(buffer, 0, length));
        updateLogTextView();
    }

    @Override
    public void onStatusChange(BluetoothStatus status) {
        Log.d(TAG, "onStatusChange: " + status);
    }

    @Override
    public void onDeviceName(String deviceName) {
        Log.d(TAG, "onDeviceName: " + deviceName);
    }

    @Override
    public void onToast(String s) {
        Log.d(TAG, "onToast");
    }

    @Override
    public void onDataWrite(byte[] buffer) {
        Log.d(TAG, "onDataWrite" + buffer);
        log.println("> " + new String(buffer));
        updateLogTextView();
    }

    public void updateLogTextView() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                logTextView.setText(logBuilder.toString());
                logScrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }
}
