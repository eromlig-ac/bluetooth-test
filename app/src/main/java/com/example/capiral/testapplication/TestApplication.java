package com.example.capiral.testapplication;

import android.app.Application;

import com.github.douglasjunior.bluetoothclassiclibrary.BluetoothClassicService;
import com.github.douglasjunior.bluetoothclassiclibrary.BluetoothConfiguration;
import com.github.douglasjunior.bluetoothclassiclibrary.BluetoothService;

import java.util.UUID;

public class TestApplication extends Application{

    private static final UUID UUID_DEVICE = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

    @Override
    public void onCreate() {
        super.onCreate();

        BluetoothConfiguration config = new BluetoothConfiguration();
        config.bluetoothServiceClass = BluetoothClassicService.class;
        config.context = getApplicationContext();
        config.bufferSize = 1024;
        config.characterDelimiter = '\n';
        config.deviceName = "Bluetooth Sample";
        config.callListenersInMainThread = false;
        config.uuid = UUID_DEVICE; // For Classic

        BluetoothService.init(config);
    }

}
